package sa.tamkeen.validations;

public class Validations {    
  //comment
	/*public static void main(String[] args) {
		doValidations();
	}*/
	public static java.util.ArrayList<String> doValidations(String idNo, String smsCode) {
		Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(idNo).matches();
		Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(idNo).matches();
		Boolean isValidSmsCode = java.util.regex.Pattern.compile("^\\d{4}").matcher(smsCode).matches();
		/*String id =String.valueOf(bDate);
		Boolean isValidBirthDate = false;
		Boolean data_validation_error_flag = false;
				if (id.toString().length() == 8) {
					String day = id.toString().substring(6, 8);
					String month = id.toString().substring(4, 6);
					String year = id.toString().substring(0, 4);

					if (Integer.parseInt(day) <= 30 && Integer.parseInt(day) != 00) {
						if (Integer.parseInt(month) <= 12 && Integer.parseInt(month) != 0) {
							if (year.startsWith("1"))
								isValidBirthDate= true;
						}
					}
				}else {
				isValidBirthDate= false;
				}*/
				
		java.util.ArrayList<String> al=new java.util.ArrayList<String>();
		if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
		if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
		if(isValidSmsCode == false) al.add("Data Validation Fail : Sms Code length should be 4  ");
		return al;
		
	}

	
	public static java.util.ArrayList<String> getCitigenInfoValidator(String idNo, String birthDate, String operatorId) {
		Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(idNo).matches();
		Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(idNo).matches();
		Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
		String id =String.valueOf(birthDate);
		Boolean isValidBirthDate = false;
		Boolean data_validation_error_flag = false;
				if (id.toString().length() == 8) {
					String day = id.toString().substring(6, 8);
					String month = id.toString().substring(4, 6);
					String year = id.toString().substring(0, 4);

					if (Integer.parseInt(day) <= 30 && Integer.parseInt(day) != 00) {
						if (Integer.parseInt(month) <= 12 && Integer.parseInt(month) != 0) {
							if (year.startsWith("1"))
								isValidBirthDate= true;
						}
					}
				}else {
				isValidBirthDate= false;
				}
				
		java.util.ArrayList<String> al=new java.util.ArrayList<String>();
		if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
		if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
		if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
		if(isValidBirthDate == false) al.add("Data Validation Fail : Birth Date Not a valid Date  ");

		return al;
	}
	

	public static java.util.ArrayList<String> QueryDependentByIdValidator(String idNo, String operatorId) {
		
		Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(2|3|4)[0-9]*").matcher(idNo).matches();
		Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(idNo).matches();
		Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
				
		java.util.ArrayList<String> al=new java.util.ArrayList<String>();
		if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 2, 3 or 4 ");
		if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
		if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
		return al;
	}

	public static java.util.ArrayList<String> CancelChangeOccupation(String IdNo, String laborOfficeId, String serialYear, String serialNumber, String operatorId) {
		
		Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
		Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
		Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
				
		java.util.ArrayList<String> al=new java.util.ArrayList<String>();
		if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
		if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
		if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
		return al;
	}
	//CancelChangeSponsor(String, String, String, String, String, String)
public static java.util.ArrayList<String> CancelChangeSponsor(String IdNo, String laborOfficeId, String serialYear, String serialNumber, String operatorId, String sponsorId) {
		
		Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
		Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
		Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
				
		java.util.ArrayList<String> al=new java.util.ArrayList<String>();
		if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
		if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
		if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
		return al;
	}
//CancelRunaway(String, String, String, String)
public static java.util.ArrayList<String> CancelRunaway(String validate, String runawayId, String reporterId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//CancelVisa(String, String, String)
public static java.util.ArrayList<String> CancelVisa(String requestNumber, String borderNumber, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//CheckVehicleCoOwner(String, String, String)
public static java.util.ArrayList<String> CheckVehicleCoOwner(String vehicleSequenceNo, String coOwnerId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//CheckVehicleOwner(String, String, String)
public static java.util.ArrayList<String> CheckVehicleOwner(String vehicleSequenceNo, String coOwnerId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//CheckVehiclePreviousOwner(String, String, String)
public static java.util.ArrayList<String> CheckVehiclePreviousOwner(String vehicleSequenceNo, String previousOwnerId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//CreateRunaway(String, String, String, String, String)
public static java.util.ArrayList<String> CreateRunaway(String validate, String runawayId, String reporterId, String dateHijri, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//Generate700ForExistingEstablishmentWithoutCR(String)
public static java.util.ArrayList<String> Generate700ForExistingEstablishmentWithoutCR(String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//GetLaborsBySponsorId(String, String, String)
public static java.util.ArrayList<String> GetLaborsBySponsorId(String sponsorId, String lastpage, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//GetLaborVisaList(String, String, String, String)
public static java.util.ArrayList<String> GetLaborVisaList(String borderNo, String VisaNo, String lastBorderNo, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//GetLabrorPrisonStatus(String, String)
public static java.util.ArrayList<String> GetLabrorPrisonStatus(String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//GetNicEstablishmentInfo(String, String)
public static java.util.ArrayList<String> GetNicEstablishmentInfo(String sponsorId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//GetNicEstablishmentInfoByCommercialRecord(String, String)
public static java.util.ArrayList<String> GetNicEstablishmentInfoByCommercialRecord(String commercialRecord, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//IssueWorkPermit(String, String, String)
public static java.util.ArrayList<String> IssueWorkPermit(String IdNo, String sponsoIdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//OpenEstablishmentFileWithoutCR(String)
public static java.util.ArrayList<String> OpenEstablishmentFileWithoutCR(String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//IssueVisa(String, String)
public static java.util.ArrayList<String> IssueVisa(String laborOfficeId, String operatorId) {
	
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//QueryDependentsByIdMOLCode(String, String)
public static java.util.ArrayList<String> QueryDependentsByIdMOLCode(String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//QueryWorkPermit(String, String, String, String)
public static java.util.ArrayList<String> QueryWorkPermit(String laborOfficeId, String licenseNo, String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//SendChangeOccupationApproval(String, String, String)
public static java.util.ArrayList<String> SendChangeOccupationApproval(String laborOfficeId, String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//UpdateAlienSponsorBusiness(String)
public static java.util.ArrayList<String> UpdateAlienSponsorBusiness(String laborOfficeId) {
	//Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(laborOfficeId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	//if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//ValidateAlienSponsorBusiness(String)
public static java.util.ArrayList<String> ValidateAlienSponsorBusiness(String laborOfficeId) {
	//Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(laborOfficeId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	//if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//ValidateIssueWorkPermit(String, String)
public static java.util.ArrayList<String> ValidateIssueWorkPermit(String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}
//SendChangeSponsorApproval(String, String, String)
public static java.util.ArrayList<String> SendChangeSponsorApproval(String laborOfficeId,String IdNo, String operatorId) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2)[0-9]*").matcher(IdNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(IdNo).matches();
	Boolean isValidOperatorId = java.util.regex.Pattern.compile("^\\d{10}").matcher(operatorId).matches();
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidOperatorId == false) al.add("Data Validation Fail : Operator Id length should be 10  ");
	return al;
}

public static java.util.ArrayList<String> getCurrentDomesticSponsereeInfo(String idNo, String birthDate) {
	Boolean isValidIdNoStarts = java.util.regex.Pattern.compile("^(1|2|3|4)[0-9]*").matcher(idNo).matches();
	Boolean isValidIdNoLength = java.util.regex.Pattern.compile("^\\d{10}").matcher(idNo).matches();
	String id =String.valueOf(birthDate);
	Boolean isValidBirthDate = false;
	Boolean data_validation_error_flag = false;
			if (id.toString().length() == 10) {
				String day = id.toString().substring(8, 10);
				String month = id.toString().substring(5, 7);
				String year = id.toString().substring(0, 4);

				if (Integer.parseInt(day) <= 30 && Integer.parseInt(day) != 00) {
					if (Integer.parseInt(month) <= 12 && Integer.parseInt(month) != 0) {
						if (year.startsWith("1"))
							isValidBirthDate= true;
					}
				}
			}else {
				isValidBirthDate= false;
			}
			
	java.util.ArrayList<String> al=new java.util.ArrayList<String>();
	if(isValidIdNoStarts == false) al.add("Data Validation Fail : IdNo should starts with 1 or 2 or 3 or 4 ");
	if(isValidIdNoLength == false) al.add("Data Validation Fail : IdNo length should be 10 ");
	if(isValidBirthDate == false) al.add("Data Validation Fail : Birth Date Not a valid Date  ");

	return al;
}

		

}
